module PotmumMailer
  class Engine < ::Rails::Engine
    isolate_namespace PotmumMailer
  end
end
