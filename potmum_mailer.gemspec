$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "potmum_mailer/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "potmum_mailer"
  s.version     = PotmumMailer::VERSION
  s.authors     = ["Yuu Kikuchi"]
  s.email       = ["yu.kikuchi@optim.co.jp"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of PotmumMailer."
  s.description = "TODO: Description of PotmumMailer."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.5.1"
end
